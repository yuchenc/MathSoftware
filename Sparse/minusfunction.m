function B=minusfunction(A,p,q) %A=rowIdx,BcolIdx,C=entri
 m=length(A.row)-1;
 B=A;
if p==q
    B.ent(B.row(p))=0;
else if p<m
  for j=B.row(p):(B.row(p+1)-1)
    if B.col(j)==q
        B.col(j)=[];
        B.ent(j)=[];
    end 
  end 
else if p==m
   for j=B.row(m):B.row(m+1)
    if B.col(j)==q
        B.col(j)=[];
        B.ent(j)=[];
    end 
   end 
    end 
end
for i=p+1:m+1
   B.row(i)=B.row(i)-1;
end
end