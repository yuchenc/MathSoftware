function B= ToFullMatrix(A)
%ToFullMatrix函数实现从三数组存储方式转换到全存储方式，返回一个矩阵
%对角线上的元素为该行第一个非零元素；row(m+1)储存矩阵列数

[rn,rm] = size(A.row);
m=rm-1;
B=zeros(m,m);

c=1;

for i=1:1:m-1
    k=A.row(i+1)-A.row(i);%获取邻行首个非零元素之间的非零元素个数
    while(k>0)
        j=A.col(c);
        B(i,j)=A.ent(c);
        c=c+1;
        k=k-1;
    end    
end

[cn,cm] = size(A.col);
while(cm>=c)
    j=A.col(c);
    B(m,j)=A.ent(c);
    c=c+1;
end

end

