function D= DotProductcao(A,B)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[rn1,rm1]=size(A.row);
[rn2,rm2]=size(B.row);
[rp1,rq1]=size(A.col);
[rp2,rq2]=size(B.col);

m=rm1-1;
n=A.col(m);
M=zeros(m,n);
N=zeros(m,n);
C=zeros(m,n);

c=1;
d=1;

if rm1~=rm2 || A.col(rq1)~=B.col(rq2)
    fprintf('进行点积的两个矩阵必须行数、列数相等。')
else 
    for i=1:1:m-1
        k=A.row(i+1)-A.row(i); %获取第一个矩阵第i行的非零元素个数
        l=B.row(i+1)-B.row(i); %获取第二个矩阵第i行的非零元素个数
        while(k>0)
             j1=A.col(c);
             M(i,j1)=A.ent(c);
             c=c+1;
             k=k-1;
        end
        while(l>0)
            j2=B.col(d);
            N(i,j2)=B.ent(d);
            d=d+1;
            l=l-1;
        end
        for j=1:1:n
            C(i,j)=M(i,j)*N(i,j);    %按行扫描生成新的行
        end
    end
    for i=m
        k=A.row(i+1)-A.row(i)+1;  %获取第一个矩阵第m行的非零元素个数
        l=B.row(i+1)-B.row(i)+1;   %获取第二个矩阵第m行的非零元素个数
        while(k>0)
             j1=A.col(c);
             M(i,j1)=A.ent(c);
             c=c+1;
             k=k-1;
        end
        while(l>0)
            j2=B.col(d);
            N(i,j2)=B.ent(d);
            d=d+1;
            l=l-1;
        end
        for j=1:1:n
            C(m,j)=M(m,j)*N(m,j);    %按行扫描生成新的行
        end
    end
end
D=ToThreeArray(C);
end

