function B = SToThreeArray(A)
% SToThreeArray函数实现从稀疏存储方式转换到三数组存储方式，返回三个数组
% 考虑方阵；row(m+1)储存方阵非零元素个数

[m,n] = size(A);%得到矩阵的行和列
k=1;%为col和ent数组下标
c=0;%记录非零元素的个数（包括对角线中的零元素）
for i=1:1:m
    c=c+1;%对角线要无条件+1
    B.ent(k)=full(A(i,i));
    B.col(k)=i;
    k=k+1;%下标递增
    B.row(i)=c;
    for j=1:1:n
        if (A(i,j)~=0)&&(i~=j)
            c=c+1;
            t=full(A(i,j));
            B.ent(k)=full(A(i,j));
            B.col(k)=j;
            k=k+1;  
        end
    end
end
B.row(i+1)=n;

end

