function A=xishu(rowIdx,colIdx,entries)
[m,n]=size(rowIdx)
A=sparse(n-1,n-1)
for i=1:n-2
    for j=rowIdx{i}:rowIdx{i+1}-1
        A(i,colIdx{j})=entries{j};
    end
end
for k=rowIdx{n-1}:rowIdx{n}
    A(n-1,colIdx{k})=entries{k};
end