function B=ToSparse(A)
%ToSparse函数实现从三数组存储方式转换到稀疏存储方式，返回一个稀疏矩阵
%考虑方阵；对角线上的元素为该行第一个非零元素，row(m+1)储存方阵非零元素个数

[m,n]=size(A.row);
B=sparse(n-1,n-1);%初始化
for i=1:n-2
    for j=A.row(i):A.row(i+1)-1
        B(i,A.col(j))=A.ent(j);
    end
end
for k=A.row(n-1):A.row(n)
    B(n-1,A.col(k))=A.ent(k);
end
end