rowIdx = { 1 3 5 8 10 11};
colIdx = { 1 5 2 4 3 2 5 4 1 5 1};
entries = { 3 1 0 6 0 4 2 0 1 4 5 };
[m,n]=size(rowIdx);
A=sparse(n-1,n-1);
for i=1:n-2
    for j=rowIdx{i}:rowIdx{i+1}-1
        A(i,colIdx{j})=entries{j};
    end
end
for k=rowIdx{n-1}:rowIdx{n}
    A(n-1,colIdx{k})=entries{k};
end
A=A