function D= TensorProduct(A,B)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[rn1,rm1]=size(A.row);
[rn2,rm2]=size(B.row);
[rp1,rq1]=size(A.col);
[rp2,rq2]=size(B.col);

m1=rm1-1;
n1=A.col(m1);
m2=rm2-1;
n2=B.col(m2);

M=zeros(m1,n1);
N=ToFullMatrix(B);
C=zeros(m1*m2,n1*n2);

c=1;

for i=1:1:m1-1
    k=A.row(i+1)-A.row(i); %获取A矩阵第i行的非零元素个数
    while(k>0)
        j=A.col(c);
        M(i,j)=A.ent(c);
        c=c+1;
        k=k-1;
    end
    for j=1:1:n1
        C((m2*(i-1)+1):(m2*i),(n2*(j-1)+1):(n2*j))=M(i,j)*N;
    end
end
for i=m1
    k=A.row(i+1)-A.row(i)+1; %获取A矩阵最后一行的非零元素个数
    while(k>0)
        j=A.col(c);
        M(i,j)=A.ent(c);
        c=c+1;
        k=k-1;
    end
    for j=1:1:n1
        C((m2*(i-1)+1):(m2*i),(n2*(j-1)+1):(n2*j))=M(i,j)*N;
    end
end
D=ToThreeArray(C);
end

