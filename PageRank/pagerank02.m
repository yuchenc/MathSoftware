function out=PageRank02(G)
[n,n]=size(G);
p=0.85;
sn=sum(G,1);     % 按列求矩阵 G 各列的列和
D=sparse(n,n);
for i=1:n
    if sn(i)==0
        D(i,i)=0;
    else
    D(i,i)=1/sn(i);
    end
end
A=p*G*D;
x(n,1)=0;
e=ones(n,1);
x(:,2)=A*x(:,1)+e
k=2;
while min(x(:,k)-x(:,k-1))>0.001
    x(:,k+1)=A*x(:,k)+e;
    k=k+1;
end
t=x(:,k);
t=t/sum(t);
[x1,index]=sort(t);
% 输出结果
out=[1:n; x1'; index'];
fprintf('迭代步数 = %d\n',k)
str1='排名'; str2='PageRank得分';str3='序号';
fprintf('%-6s %-15s %-5s\n',str1,str2,str3);
fprintf('% -6d %-15f %-5d\n',out);

%A=p*G*D + delta;
% 幂迭代法
%x=ones(n,1)/n; % 迭代初始向量
%z=zeros(n,1);
%cnt=0;  % 用于记录迭步数
%while max(abs(x-z)) > 0.0001 
  % z = x;
  % x = A*x;
  % cnt=cnt+1;
%end
%[x1,index]=sort(x);
%x1=flipud(x1);
%index=flipud(index);
% 输出结果
%out=[1:n; x1'; index'];
%fprintf('迭代步数 = %d\n',cnt)
%str1='排名'; str2='PageRank得分';str3='序号';
%fprintf('%-6s %-15s %-5s\n',str1,str2,str3);
%fprintf('% -6d %-15f %-5d\n',out);